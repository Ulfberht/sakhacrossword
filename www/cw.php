<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Сахалыы кроссворд</title>
    <meta name="keywords" content="Кроссворд, саха, сахалыы" />
    <meta name="description" content="Сахалыы кроссвордар" />
    
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/app.css">
  </head>
  <body>
      <?php
        include 'header.inc';
      ?>
      
      <div class="container">
        <div class="content">
          <div class="row">
            <div class='cw-name centered'>Кроссворд #</div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-lg-6" style="background-color: white">
              <!-- <div class="table-responsive centered"> -->
                <div class="cw-table-wrapper">
                <!-- <table id="cw-table" border="1" class="table table-bordered centered"> -->
                  <table id="cw-table" class="cw-table" border="1">
                 <!--   <tr>
                    <td>
                      <input maxlength="1" type="text" id="19-down-6" class="active" onfocus="highlightWord('19-down', '6');"></input>
                    </td>
                 </tr> -->
                </table>
              </div>
              <div class="centered">
                <input type="button" class='btn-compare-all cw-btn' value='Тэҥнээ'></input> 
                <input type="button" class='btn-show-answers cw-btn' value='Хоруйдар'></input> 
                <input type="button" class='btn-clear-all cw-btn' value='Ыраастаа'></input> 
              </div>
              <p class="centered">'<b>Ҥ</b>' буукубаны суруйарга клавиатураҕа <kbd>4</kbd> сыыппараны баттаа. <br>
              '<b>Ҕ</b>' буукубаҕа: <kbd>5</kbd>. '<b>Ө</b>' буукубаҕа: <kbd>6</kbd>. '<b>Һ</b>' буукубаҕа: <kbd>7</kbd>. '<b>Ү</b>' буукубаҕа: <kbd>8</kbd>.</p>
            </div>
            <div class="col-xs-12 col-lg-3" style="background-color: white">
              <div class="horizontal-clues">
                <b>Сытыары →</b>
              </div>
            </div>
            <div class="col-xs-12 col-lg-3" style="background-color: white">
              <div class="vertical-clues">
                <b>Туруору ↓</b>
              </div>
            </div>        
          </div>
          </div>
      </div>
      <?php
        require 'footer.inc';
      ?>
    <script type="text/javascript">
    var uid = '181015';
    var wid = '389089';
    </script>
    <script type="text/javascript" src="//cdn.popcash.net/pop.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/app.js"></script>
  </body>
</html>