<?php
  class Word {
    public $num;
    public $direction;
    public $x;
    public $y;
    public $length;
    public $clue;
    public $solution;
    
  }
  $cw_num = $_REQUEST["cw_num"];

  if (!($cw_num >= 1 && $cw_num <= 100))
    $cw_num = 0;

  // $filename = "cw_" . $cw_num .".ini";
  // $file = fopen($filename, "r");
  // $text = fread($file, filesize($filename));
  // fclose($file);

  $db = new PDO("mysql:host=localhost;dbname=sakhacrossword;charset=utf8", "sakhacrossword", "JYVESgrbb43");  
  $stmt = $db->prepare("SELECT val FROM cw WHERE id = ?");
  $stmt->execute([$cw_num]);
  $res = $stmt->fetch();
  $text = $res["val"];

  $text = strstr($text, "\n[");
  $text = trim($text, "\n");
  // echo($text);
  $text_exploded = explode("\n\n", $text);
  
  $words = [];

  foreach ($text_exploded as $piece) {
    // echo($piece);  
    // echo(nl2br("\n\n\n\n\n"));
    $result = [];
    preg_match("/\[(\d+)\-/", $piece, $result);
    $num = $result[1];
    preg_match("/\-([a-z]+)\]/", $piece, $result);
    $direction = $result[1];
    preg_match("/x\=(\d+)/", $piece, $result);
    $x = $result[1] - 1;
    // $x = $result[1] - 1;
    preg_match("/y\=(\d+)/", $piece, $result);
    $y = $result[1] - 1;
    // $y = $result[1] - 1;
    preg_match("/length\=(\d+)/", $piece, $result);
    $length = $result[1];
    preg_match("/clue\=(.+)/", $piece, $result);
    $clue = $result[1];
    preg_match("/solution\=(.+)/", $piece, $result);
    $solution = $result[1];

    $word = new Word();
    $word->num = $num;
    $word->x = $x;
    $word->direction = $direction;
    $word->y = $y;
    $word->length = $length;
    $word->clue = trim($clue, " \"");
    $word->solution = trim($solution, " \"");
    array_push($words, $word);
  }
  printf("%s", json_encode($words));
?>