<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Сахалыы кроссворд</title>
    <meta name="keywords" content="Кроссворд, саха, сахалыы" />
    <meta name="description" content="Сахалыы кроссвордар" />
    <meta name="yandex-verification" content="38754ab6529ee5ca" />

    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/app.css">
  </head>
  <body>
    <?php
        include 'header.inc';
    ?>
    <div class="container">
      <div class="content">
        <h1 class="centered">Кроссворду тал:</h1>
        <ul class="cw-list">
          <?php
            $db = new PDO("mysql:host=localhost;dbname=sakhacrossword;charset=utf8", "sakhacrossword", "JYVESgrbb43");  
            $stmt = $db->query("SELECT id FROM cw");
            $cw_ids = [];
            while ($row = $stmt->fetch()) {
              echo "<li><a href=\"/cw.php?cw_num=" . $row['id'] . "\">" . "Кроссворд #" . $row['id'] . "</a></li>";
              echo "<br>";
            }
          ?>
        </ul>
      </div>
    </div>
    <?php
      require 'footer.inc';
    ?>
    <!-- <script type="text/javascript">
    var uid = '181015';
    var wid = '389089';
    </script>
    <script type="text/javascript" src="//cdn.popcash.net/pop.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/app.js"></script>
  </body>
</html>