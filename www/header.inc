<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-10">
                    <a class="navbar-brand" href="/">Сахалыы кроссворд</a>
                </div>
                <div class="col-md-4 hidden-xs"></div>
                <div class="col-md-4 col-xs-2">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a title="Случайнай кроссворд" class="btn-random hidden-xs" href="#">Случайнай</a>
                            <a title="Случайнай кроссворд" class="btn-random visible-xs" href="#"><span class="glyphicon glyphicon-random"></span></a>
                        </li>
                    </ul>                    
                </div>
            </div>
    </div>
</nav>
