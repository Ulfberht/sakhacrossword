var cw;
  var d = "A";
  var current_word_num;
  var current_word_direction;
  
  var cw_num = getParameterByName('cw_num');

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

  function init()
  {
    $.ajax({
      url: "getcwdata.php", //Relative or absolute path to response.php file
      type: "GET",
      dataType: "json",      
      data: {"cw_num": cw_num},
      success: function(content) {
        cw = content;
        render();
        bind();
      }
    });
  }

  $(document).ready(function() {
    init();
  });

  // $(document).on('pageinit', function() {
  //   init();
  // });

  function getWidth() {
    return 20;
  }

  function getHeight() {
    return 20;
  }

  function bind () {
    // binds for keyboard events
    $('td').on('keydown', function(event) {
      event.preventDefault();
      var col = $(this).index();
      
      // up
      if (event.which == 38) {
        $(this).closest('tr').prev().children().eq(col).find('input').focus();
      }
      
      // down
      else if (event.which == 40) {
        $(this).closest('tr').next().children().eq(col).find('input').focus();
      }
      
      // left
      else if (event.which == 37) {
        $(this).prev().find('input').focus();
      }

      // right
      else if (event.which == 39) {
        $(this).next().find('input').focus();
      }

      else if ((event.which >= 65 && event.which <= 91) || (event.which >= 52 && event.which <= 56) || event.which == 188 || event.which == 190 || event.which == 219 || event.which == 221 || event.which == 186 || event.which == 222) {
        $(this).find('input').val(get_letter(event.which));
        // добавить сюда проверку является ли слово вертикальным или горизонтальным
        // console.log("current_word_direction:" + current_word_direction);
        if (current_word_direction == "across")
          $(this).next().find('input').focus();
        else if (current_word_direction == "down")
          $(this).closest('tr').next().children().eq(col).find('input').focus();
      }
    });

    $('td input').on('click', function() {      
      $('#cw-table input').removeClass('highlighted');

      if ($(this).attr('data-word-direction') && $(this).attr('data-second-word-direction')) {
        // console.log('intersection');
        if (d == "A") {
          // console.log("going A");
          // input.data-word-direction == thisinput.data-word direction && input.data-word-num == thisinput.data-word-num
          $('#cw-table input[data-word-direction=' + $(this).attr('data-word-direction') + '][data-word-num=' + $(this).attr('data-word-num') + ']').addClass('highlighted');
          // input.data-word-direction2 == thisinput.data-word direction && input.data-word-num2 == thisinput.data-word-num
          $('#cw-table input[data-second-word-direction=' + $(this).attr('data-word-direction') + '][data-second-word-num=' + $(this).attr('data-word-num') + ']').addClass('highlighted');
          d = "B";
          $('.clue').removeClass('highlighted');
          $('.clue[data-word-direction=' + $(this).attr('data-word-direction') + '][data-word-num=' + $(this).attr('data-word-num') + ']').addClass('highlighted');
          current_word_direction = $(this).attr('data-word-direction');
          current_word_num = $(this).attr('data-word-num');
        }
        else if (d == "B") {
          // console.log("going B");
          // input.data-word-direction == thisinput.data-word2 direction && input.data-word-num == thisinput.data-word-num2
          $('#cw-table input[data-word-direction=' + $(this).attr('data-second-word-direction') + '][data-word-num=' + $(this).attr('data-second-word-num') + ']').addClass('highlighted');     
          // input.data-word-direction2 == thisinput.data-word2 direction && input.data-word-num2 == thisinput.data-word-num2
          $('#cw-table input[data-second-word-direction=' + $(this).attr('data-second-word-direction') + '][data-second-word-num=' + $(this).attr('data-second-word-num') + ']').addClass('highlighted');
          d = "A";
          $('.clue').removeClass('highlighted');
          $('.clue[data-word-direction=' + $(this).attr('data-second-word-direction') + '][data-word-num=' + $(this).attr('data-second-word-num') + ']').addClass('highlighted');
          current_word_direction = $(this).attr('data-second-word-direction');
          current_word_num = $(this).attr('data-second-word-num');
        }
      }
      else {
        // console.log('solo');
        // input.data-word-direction == thisinput.data-word direction && input.data-word-num == thisinput.data-word-num
        $('#cw-table input[data-word-direction=' + $(this).attr('data-word-direction') + '][data-word-num=' + $(this).attr('data-word-num') + ']').addClass('highlighted');

        // input.data-word-direction2 == thisinput.data-word direction && input.data-word-num2 == thisinput.data-word-num
        $('#cw-table input[data-second-word-direction=' + $(this).attr('data-word-direction') + '][data-second-word-num=' + $(this).attr('data-word-num') + ']').addClass('highlighted');
        $('.clue').removeClass('highlighted');
        $('.clue[data-word-direction=' + $(this).attr('data-word-direction') + '][data-word-num=' + $(this).attr('data-word-num') + ']').addClass('highlighted');
        current_word_direction = $(this).attr('data-word-direction');
        current_word_num = $(this).attr('data-word-num');
        }
        
    });

    // binds for mouse events
    $('.clue').on('mousedown', function() {
      // console.log("clue clicked");
      $('.clue').removeClass('highlighted');
      $(this).addClass('highlighted');

      $('input').removeClass('highlighted');
      $('input[data-word-direction=' + $(this).attr('data-word-direction') + '][data-word-num=' + $(this).attr('data-word-num') + '],' + 'input[data-second-word-direction=' + $(this).attr('data-word-direction') + '][data-second-word-num=' + $(this).attr('data-word-num') + ']').addClass('highlighted');
      
      let w = -1;
      for (i = 0; i < cw.length; i++) {
        if (cw[i].num == $(this).attr('data-word-num') && cw[i].direction == $(this).attr('data-word-direction')) {
          w = i;
          break;
        }
      }

      let $cell = $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x)).find('input').first();
      // console.log($cell);
      current_word_direction = cw[w].direction;
      current_word_num = cw[w].num;
      setTimeout(function(){
        $cell.focus();
        //$cell.setSelectionRange(0, 1);
      }, 1);
      
    });

    $('.btn-show-answers').on('click', function() {
      showAnswers();
    });
    $('.btn-clear-all').on('click', function() {
      clearAll();
    });
    $('.btn-compare-all').on('click', function() {
      compareAll();
    });
    $('.btn-random').on('click', function() {
      $.ajax({
      url: "getcwlist.php", //Relative or absolute path to response.php file
      type: "GET",
      dataType: "json",      
      data: {},
      success: function(content) {
        let cw_ids = content;
        let random_id = cw_ids[Math.floor(Math.random()*cw_ids.length)];
        window.location.replace("/cw.php?cw_num=" + random_id);
      }
    });
    });
  }

  // Возвращает случайное число между min и max включительно
  function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
  }

  function compareAll() {
    // compare char to char and mark with color
    // for (w = 0; w < cw.length; w++) {
    //   if (cw[w].direction == "across") {
    //     for (j = 0; j < cw[w].length; j++) {
    //       $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').removeClass('correct');
    //       $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').removeClass('incorrect');
    //       if ($('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').val() == cw[w].solution[j].toUpperCase())
    //         $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').addClass('correct');
    //       else
    //         $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').addClass('incorrect');
    //     }
    //   }

    //   if (cw[w].direction == "down") {
    //     for (i = 0; i < cw[w].length; i++) {
    //       $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').removeClass('correct');
    //       $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').removeClass('incorrect');
    //       if ($('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').val() == cw[w].solution[i].toUpperCase())
    //         $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').addClass('correct');
    //       else
    //         $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').addClass('incorrect');
    //     }
    //   }
    // }

    // compare char to char and remove incorrect
    for (w = 0; w < cw.length; w++) {
      if (cw[w].direction == "across") {
        for (j = 0; j < cw[w].length; j++) {
          if ($('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').val() != cw[w].solution[j].toUpperCase())
            $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').val('');
        }
      }

      if (cw[w].direction == "down") {
        for (i = 0; i < cw[w].length; i++) {
          if ($('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').val() != cw[w].solution[i].toUpperCase())
            $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').val('');
        }
      }
    }

    //compare word to word

    // for (w = 0; w < cw.length; w++) {
    //   let correct = true;
    //   if (cw[w].direction == "across") {
        
    //     for (j = 0; j < cw[w].length; j++) {
    //       if ($('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').val() != cw[w].solution[j].toUpperCase())
    //         correct = false;
    //     }
    //     for (j = 0; j < cw[w].length; j++) {
    //       $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').removeClass('correct');
    //       $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').removeClass('incorrect');
    //       if (correct)
    //         $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').addClass('correct');          
    //       else
    //         $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').addClass('incorrect');
    //     }
    //   }
      

    //   if (cw[w].direction == "down") {
    //     for (i = 0; i < cw[w].length; i++) {
    //       if ($('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').val() != cw[w].solution[i].toUpperCase())
    //         correct = false;
    //     }
    //     for (i = 0; i < cw[w].length; i++) {
    //       $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').removeClass('correct');
    //       $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').removeClass('incorrect');
    //       if (correct)
    //         $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').addClass('correct');
    //       else
    //         $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').addClass('incorrect');
    //     }
    //   }
    // }
  }

  function showAnswers() {
    for (w = 0; w < cw.length; w++) {
      if (cw[w].direction == "across") {
        for (j = 0; j < cw[w].length; j++) {
          $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').removeClass('correct');
          $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').removeClass('incorrect');
          $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').val(cw[w].solution[j].toUpperCase());
        }
      }

      if (cw[w].direction == "down") {
        for (i = 0; i < cw[w].length; i++) {
          $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').val(cw[w].solution[i].toUpperCase());
          $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').removeClass('correct');
          $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').removeClass('incorrect');
        }
      }
    }
  }

  function clearAll() {
    $('input.input-cell').val('');
    $('input.input-cell').removeClass('correct');
    $('input.input-cell').removeClass('incorrect');
  }

  function render() {
    $('.cw-name').html('Кроссворд #' + cw_num);
    // draw table with empty cells (without input)
    for (i = 0; i < getHeight(); i++) {
      let rowstr = '';
      for (j = 0; j < getWidth(); j++)
        rowstr = rowstr + '<td></td>';
      $('#cw-table').append('<tr>' + rowstr + '</tr>');
    }

    // draw inputs
    for (w = 0; w < cw.length; w++) {
      var la = '<div class="la">' + cw[w].num + '</div>';
      if (cw[w].direction == "across") {
        for (j = 0; j < cw[w].length; j++) {
          if ($('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').length)
            $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('input').attr('data-second-word-num', cw[w].num).attr('data-second-word-direction', cw[w].direction);
          else
            $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).css({"background-color": "white"}).html('<div class="cell">' + '<input maxlength="1" type="text" class="input-cell" data-word-num="' + cw[w].num + '" data-word-direction="' + cw[w].direction + '" ></input>' + '</div>');
          // if it's first cell in the word, print the word number
          if (j == 0)
            if ($('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('.cell').find('.la').length == 0)
              $('#cw-table tr').eq(Number(cw[w].y)).find('td').eq(Number(cw[w].x) + Number(j)).find('.cell').prepend(la);
        }
      }

      if (cw[w].direction == "down") {
        for (i = 0; i < cw[w].length; i++) {
          if ($('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').length)
            $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('input').attr('data-second-word-num', cw[w].num).attr('data-second-word-direction', cw[w].direction);
          else          
            $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).css({"background-color": "white"}).html('<div class="cell">' + '<input maxlength="1" type="text" class="input-cell" data-word-num="' + cw[w].num + '" data-word-direction="' + cw[w].direction + '" ></input>' + '</div>');
          // if it's first cell in the word, print the word number
          if (i == 0)
            if ($('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('.cell').find('.la').length == 0)
              $('#cw-table tr').eq(Number(cw[w].y) + Number(i)).find('td').eq(Number(cw[w].x)).find('.cell').prepend(la);
        }
      }
    }
    
    // draw clues
    for (w = 0; w < cw.length; w++) {
      if (cw[w].direction == "across") {
        $('.horizontal-clues').append('<div class="clue" data-word-num="' + cw[w].num + '" data-word-direction="' + cw[w].direction + '"><b>' + cw[w].num + '.</b> ' + cw[w].clue + ' </div>');
      }

      if (cw[w].direction == "down") {
       $('.vertical-clues').append('<div class="clue" data-word-num="' + cw[w].num + '" data-word-direction="' + cw[w].direction + '"><b>' + cw[w].num + '.</b> ' + cw[w].clue + ' </div>');
      }
    }    
  }



  function highlightWord () {

  }

  function get_letter(which) {
  switch(which) {
  case 65:
    return 'Ф';
  case 66:
    return 'И';
  case 67:
    return 'С';
  case 68:
    return 'В';
  case 69:
    return 'У';
  case 70:
    return 'А';
  case 71:
    return 'П';
  case 72:
    return 'Р';
  case 73:
    return 'Ш';
  case 74:
    return 'О';
  case 75:
    return 'Л';
  case 76:
    return 'Д';
  case 77:
    return 'Ь';
  case 78:
    return 'Т';
  case 79:
    return 'Щ';
  case 80:
    return 'З';
  case 81:
    return 'Й';
  case 82:
    return 'К';
  case 83:
    return 'Ы';
  case 84:
    return 'Е';
  case 85:
    return 'Г';
  case 86:
    return 'М';
  case 87:
    return 'Ц';
  case 88:
    return 'Ч';
  case 89:
    return 'Н';
  case 90:
    return 'Я';
  case 91:
    return 'Х';
  case 52:
    return 'Ҥ';
  case 53:
    return 'Ҕ';
  case 54:
    return 'Ө';
  case 55:
    return 'Һ';
  case 56:
    return 'Ү';
  case 188:
    return 'Б';
  case 190:
    return 'Ю';
  case 219:
    return 'Х';
  case 221:
    return 'Ъ';
  case 186:
    return 'Ж';
  case 222:
    return 'Э';
  }
}
